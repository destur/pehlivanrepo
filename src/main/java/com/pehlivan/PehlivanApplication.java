package com.pehlivan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PehlivanApplication {

	public static void main(String[] args) {
		SpringApplication.run(PehlivanApplication.class, args);
	}
}
